/* @flow */


/**
 * Creates a standard action creator.
 * @param  {string} type     Value of type field in {@link object} returned by action creator.
 * @param  {...string} argNames Fields in the {@link object}
 * @return {Function}          The action creator.
 */
export function makeActionCreator(type: string, ...argNames: Array<string>): (...args: Array<any>) => Object {
  return function (...args: Array<any>): Object {
    const action = { type };
    argNames.forEach((arg: any, index: any) => {
      action[argNames[index]] = args[index];
    });
    return action;
  };
}


export const SET_SEARCH_TEXT = 'SET_SEARCH_TEXT';
export const setSearchText = makeActionCreator(SET_SEARCH_TEXT, 'text');

export const SET_POSTS = 'SET_POSTS';
export const setPosts = makeActionCreator(SET_POSTS, 'posts');

export const SET_NONCE = 'SET_NONCE';
export const setNonce = makeActionCreator(SET_NONCE, 'nonce');

export const SET_PAGE_LOADING = 'SET_PAGE_LOADING';
export const setPageLoading = makeActionCreator(SET_PAGE_LOADING, 'pageLoading');
