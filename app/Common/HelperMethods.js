/* @flow */

const constructorName = 'constructor';
/**
 * Binds the passed in reference _this as this to all properties in _this.
 * @param  {object}  _this class to bind this to in methods
 */
export function bindThis(_this: Object) {
  for (const name of Object.getOwnPropertyNames(Object.getPrototypeOf(_this))) {
    if (name !== constructorName) {
      _this[name] = _this[name].bind(_this);
    }
  }
}

type Header = {
  name: string,
  value: string,
};

/**
 * Creates a {@link XMLHttpRequest} according the options entered.
 * Opens the {@link XMLHttpRequest}.
 * @param  {object} options Options for creating the {@link XMLHttpRequest}
 * @param {string} options.method HTTP method
 * @param {string} options.url URL to send/receive info from
 * @param {string} options.contentType Content-Type header
 * @param {Array<Header>} options.headers A list of request headers
 * @param {successCallback} options.success
 * @param {errorCallback} options.error An error returned from the server.
 * @param {errorCallback} options.ajaxError An error in sending or receiving.
 * @return {XMLHttpRequest} The {@link XMLHttpRequest created}
 */
export function ajax(options: {
  method: string,
  url: string,
  contentType: string,
  success: (response: string | Object) => void,
  error: (error: ?string | ?Object) => void,
  ajaxError: (error: string | Object) => void,
}): ?XMLHttpRequest {
  if (!options.method) {
    // console.error('No method was supplied.');
    return null;
  }


  if (!options.url) {
    // console.error('No url was supplied.');
    return null;
  }


  var request = new XMLHttpRequest();
  request.open(options.method, options.url, true);
  if (options.contentType) {
    request.setRequestHeader('Content-Type', options.contentType);
  }
  if (options.headers && options.headers.length > 0) {
    options.headers.forEach((header: Header) => {
      request.setRequestHeader(header.name, header.value);
    });
  }
  request.onload = () => {
    try {
      var response = JSON.parse(request.responseText);
      if (request.status >= 200 && request.status < 400) {
        if (typeof options.success === 'function') {
          options.success(response);
        }
      } else {
        if (typeof options.error === 'function') {
          response.status = request.status;
          options.error(response);
        }
      }
    } catch (e) {
      console.error(e);
      options.error();
    }
  };
  request.onerror = () => {
    if (typeof options.error === 'function') {
      options.error(JSON.parse(request.responseText));
    }
  };
  if (options.data) {
    request.send(options.data);
  } else {
    request.send();
  }
  return request;
}
