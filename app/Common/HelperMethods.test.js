/* globals test */

import { bindThis } from './HelperMethods';

test('method can be accessed from this', () => {
  const testMethodValue = 'testMethod value';
  class TestClass {
    constructor() {
      expect(this).not.toHaveProperty('testMethod');
      expect(this.testMethod()).not.toBe(testMethodValue);
      bindThis(this);
      expect(this).toHaveProperty('testMethod');
      expect(this.testMethod()).toBe(testMethodValue);
    }
    testMethod() {
      return testMethodValue;
    }
  }
});
