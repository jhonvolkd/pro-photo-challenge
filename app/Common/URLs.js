/* @flow */

const posts = '/wp-json/wp/v2/posts';
const urls = {
  lastFivePosts: (): string => {
    return `${posts}?page=1&per_page=5`;
  },
  post: (id: number): string => {
    return `${posts}/${id}`;
  },
};
export default urls;
