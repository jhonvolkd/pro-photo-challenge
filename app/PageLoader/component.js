/* @flow */
import React from 'react';
import CircularProgress from 'material-ui/CircularProgress';
import { ajax, bindThis } from 'Common/HelperMethods';
import URLs from 'Common/URLs';
import { WPPost } from 'Common/types';
import './PageLoader.less';

type DefaultProps = {};

type Props = {
  pageLoading: boolean,
  setPosts: (posts: Array<WPPost>) => void,
};

type State = {};

export default class PageLoaderComponent extends React.Component<DefaultProps, Props, State> {
  defaultProps: DefaultProps;
  state: State;

  constructor(props: Props) {
    super(props);
    bindThis(this);
  }

  componentDidMount() {
    this.checkPageLoadingStatus();
  }

  componentDidUpdate() {
    this.checkPageLoadingStatus();
  }

  checkPageLoadingStatus() {
    if (this.props.pageLoading) {
      this.callAPI();
    }
  }

  callAPI() {
    ajax({
      method: 'GET',
      url: URLs.lastFivePosts(),
      success: (posts: Array<WPPost>) => {
        this.props.setPosts(posts);
      },
      error: (error: Object) => {
        console.error('Error loading posts', error);
      },
    });
  }

  render(): React$Element<*> {
    let progress;

    if (this.props.pageLoading) {
      progress = (
        <div className="loading-indicator">
          <CircularProgress />
        </div>
      );
    }

    return (
      <div>
        {progress}
        <div className={`posts-loading-children ${this.props.pageLoading ? 'loading' : 'loaded'}`}>{this.props.children}</div>
      </div>
    );
  }
}
