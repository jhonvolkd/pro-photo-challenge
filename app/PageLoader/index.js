import { connect } from 'react-redux';
import PageLoaderComponent from './component';
import { setPageLoading, setPosts } from 'Actions';

const mapStateToProps = (state) => {
  return {
    pageLoading: state.pageLoading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setPosts(posts) {
      dispatch(setPosts(posts));
      dispatch(setPageLoading(false));
    },
  };
};

const PageLoader = connect(mapStateToProps, mapDispatchToProps)(PageLoaderComponent);
export default PageLoader;
