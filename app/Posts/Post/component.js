/* @flow */
import React from 'react';
import { WPPost } from 'Common/types';
import TextField from 'material-ui/TextField';
import { ajax, bindThis } from 'Common/HelperMethods';
import URLs from 'Common/URLs';
import './Post.less';

type DefaultProps = {};

type Props = {
  nonce: string,
  reloadPosts: () => void,
} & WPPost;

type State = {
  featuredMediaHref: string,
  edittingPostTitle: boolean,
};

/**
 * Displays information on a WordPress post.
 * Allows a user to edit the title of the post and delete the post entirely.
 */
export default class PostComponent extends React.Component<DefaultProps, Props, State> {
  defaultProps: DefaultProps;
  state: State;

  constructor(props: Props) {
    super(props);
    this.state = {
      featuredMediaHref: '',
      edittingPostTitle: false,
      postTitle: this.props.title.rendered,
    };
    bindThis(this);
  }

  /**
  * This method is called if the WPPost data indicates that this post has an associated featured media.
  * This method makes an ajax call to fetch the media and sets it in state when returned;
  *
  * @param {string} url The url for the ajax call.
  */
  loadFeaturedMedia(url: string) {
    ajax({
      method: 'GET',
      url,
      success: (data: Object) => {
        this.setState((): Object => {
          return {
            featuredMediaHref: data.source_url,
          };
        });
      },
      error: (error: Object) => {
        console.error('error fetching featured media', error);
      },
    });
  }

  /**
  * Called when the edit title text is clicked on.
  * Transforms edit-post-title into a text box.
  * @param  {[type]} React$Elemant [description]
  * @return {[type]}               [description]
  */
  onEditTitleClicked() {
    this.setState((prevState: State): Object => {
      return {
        edittingPostTitle: !prevState.edittingPostTitle,
      };
    });
  }

  /**
   * Saves the editted post.
   */
  onTitledSaved() {
    ajax({
      method: 'POST',
      url: `${URLs.post(this.props.id)}?title=${this.state.postTitle}`,
      contentType: 'application/json',
      headers: [{
        name: 'X-WP-Nonce',
        value: this.props.nonce,
      }],
      success: () => {
        this.setState((): Object => {
          return {
            edittingPostTitle: false,
          };
        });
        this.props.reloadPosts();
      },
      error: (error: Object) => {
        console.error('edit error', error);
      },
    });
  }

  /**
   * Handler for edit post title TextField.
   * @param  {Event} event     Event from onChange.
   * @param  {string} postTitle New Value for postTitle
   */
  onPostTitleChange(event: Event, postTitle: string) {
    this.setState((): Object => {
      return {
        postTitle,
      };
    });
  }

  deletePost() {
    ajax({
      method: 'DELETE',
      url: URLs.post(this.props.id),
      headers: [{
        name: 'X-WP-Nonce',
        value: this.props.nonce,
      }],
      success: () => {
        this.props.reloadPosts();
      },
      error: (error: Object) => {
        console.error('edit error', error);
      },
    });
  }

  render(): React$Elemant<*> {
    let featuredMedia = '';
    if (this.state.featuredMediaHref.length > 0) {
      featuredMedia = <p className="post-featured-media" key="post-featured-media"><img src={this.state.featuredMediaHref} alt="featuredMedia" /></p>;
    }
    if (this.state.featuredMediaHref.length === 0 && this.props._links.hasOwnProperty('wp:featuredmedia')) {
      const featuredMediaObject = this.props._links['wp:featuredmedia'][0];
      if (featuredMediaObject.embeddable) {
        this.loadFeaturedMedia(featuredMediaObject.href);
      }
    }
    let postTitle = '';
    if (this.state.edittingPostTitle) {
      postTitle = (
        <TextField
          className="post-title-input"
          underlineShow={false}
          hintText="Edit Post"
          value={this.state.postTitle}
          onChange={this.onPostTitleChange}
        />
      );
    } else {
      postTitle = (<span className="post-title" key={`post-title${this.props.id}`}>{this.props.title.rendered}</span>);
    }
    return (
      <div className="post" key={this.props.id}>
        <div className="post-header" key={`post-header${this.props.id}`}>
          {postTitle}
          <span className="edit-post-title" onClick={this.state.edittingPostTitle ? this.onTitledSaved : this.onEditTitleClicked} key={`edit-post-title${this.props.id}`}>
            {this.state.edittingPostTitle ? 'Save' : 'Edit'}
          </span>
          <span className="delete-post" onClick={this.deletePost} key={`delete-post${this.props.id}`}>Delete</span>
        </div>
        {featuredMedia}
        <div className="post-content" dangerouslySetInnerHTML={{ __html: this.props.content.rendered }} key={`post-content${this.props.id}`} />
      </div>
    );
  }
}
