import { connect } from 'react-redux';
import PostComponent from './component';
import { setPageLoading } from 'Actions';


const mapStateToProps = (state) => {
  return {
    nonce: state.nonce,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    reloadPosts() {
      dispatch(setPageLoading(true));
    },
  };
};

const Post = connect(mapStateToProps, mapDispatchToProps)(PostComponent);
export default Post;
