/* @flow */
import React from 'react';
import Post from './Post';
import { WPPost } from 'Common/types';
import './Posts.less';

type DefaultProps = {};

type Props = {
  posts: Array<WPPost>,
};

type State = {

};

/**
 * Displays a list of Post elements.
 */
export default class PostsComponent extends React.Component<DefaultProps, Props, State> {
  defaultProps: DefaultProps;
  state: State;

  render(): React$Element<*> {
    return (
      <div className="posts">
        {this.props.posts.map((post: WPPost): React$Element<*> => {
          return <Post {...post} key={post.id} />;
        })}
      </div>
    );
  }
}
