import { connect } from 'react-redux';
import PostsComponent from './component';

const mapStateToProps = (state) => {
  return {
    posts: state.posts,
  };
};

const Posts = connect(mapStateToProps)(PostsComponent);
export default Posts;
