import {
  SET_SEARCH_TEXT,
  SET_POSTS,
  SET_NONCE,
  SET_PAGE_LOADING,
} from 'Actions';

export const searchText = (state = '', action) => {
  switch (action.type) {
    case SET_SEARCH_TEXT:
      return action.text;
    default:
      return state;
  }
};

export const posts = (state = [], action) => {
  switch (action.type) {
    case SET_POSTS:
      return action.posts;
    default:
      return state;
  }
};

export const nonce = (state = '', action) => {
  switch (action.type) {
    case SET_NONCE:
      return action.nonce;
    default:
      return state;
  }
};

export const pageLoading = (state = '', action) => {
  switch (action.type) {
    case SET_PAGE_LOADING:
      return action.pageLoading;
    default:
      return state;
  }
};
