import { combineReducers } from 'redux';
import { posts, nonce, pageLoading } from './ProPhotoReducers';

const proPhotoReducers = combineReducers({
  posts,
  nonce,
  pageLoading,
});

export default proPhotoReducers;
