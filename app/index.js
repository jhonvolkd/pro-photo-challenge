/* globals wpApiSettings */
/* @flow */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import Posts from './Posts';
import PageLoader from './PageLoader';
import proPhotoReducers from 'Reducers';
// import URLs from 'Common/URLs';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
// import { WPPost } from 'Common/types';
// import { ajax } from 'Common/HelperMethods';
import './App.less';

// Needed for onTouchTap
// Can go away when react 1.0 release
// Check this repo:
// https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();

window.onload = () => {
  const store = createStore(proPhotoReducers, {
    nonce: wpApiSettings.nonce,
    pageLoading: true,
  });

  ReactDOM.render(
    <Provider store={store}>
      <MuiThemeProvider>
        <PageLoader>
          <div>
	  		<h1 id="plugin-header">Pro Photo Challenge Plugin</h1>
            <Posts />
          </div>
        </PageLoader>
      </MuiThemeProvider>
    </Provider>,
    document.getElementById('react-content'),
  );
};
