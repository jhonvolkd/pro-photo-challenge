<?php
  /*
  Plugin Name: ProPhoto Challenge
  */

		
	class wp_pro_photo_challenge {
		static function echo_div() {
		}
		static function enqueue_scripts() {
			wp_enqueue_script('pro-photo-js', plugins_url('build/ProPhoto.js', __FILE__ ));
			wp_enqueue_style('pro-photo-css', plugins_url('build/ProPhoto.css', __FILE__ )); 
			wp_localize_script( 'pro-photo-js', 'wpApiSettings', array(
				'root' => esc_url_raw( rest_url() ),
				'nonce' => wp_create_nonce( 'wp_rest' )
			) );
		}
		static function add_div_and_data() {
			echo '<div id="react-content"></div>';
			// $response = wp_remote_get('/wp-json/wp/v2/posts?page=1&per_page=5');
			// $response = json_encode(get_posts(array ('numberposts' => 5)));
			// echo '<script type="text/javascript"> var PostsData = '.$response.';</script>';
		}
	}

	//add_action('wp_echo_div', array( 'wp_pro_photo_challenge', 'echo_div'));
	add_action('wp_footer', array( 'wp_pro_photo_challenge', 'add_div_and_data'));
	add_action('wp_enqueue_scripts', array( 'wp_pro_photo_challenge', 'enqueue_scripts'));
?>
