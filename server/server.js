/*
    A local Koa server for testing Pro Photo plugin seperate from WordPress.
*/
const Koa = require('koa');
const KoaStatic = require('koa-static');
const Router = require('koa-router');
const fs = require('fs');
const path = require('path');

const port = 3000;

// read json files now
// they won't change while server is running, so there's no point reading them every time the endpoint is called
const posts25 = fs.readFileSync(path.join(__dirname, 'Posts2-5.json'), 'utf8');
const editSuccessful = fs.readFileSync(path.join(__dirname, 'EditSuccessful.json'), 'utf8');

const app = new Koa();
const router = new Router();

function generateHTML(name) {
  return `
  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>${name}</title>
      <link href="https://fonts.googleapis.com/css?family=Roboto:700,500,400,300,100" rel="stylesheet" type="text/css">
    </head>
    <body>
      <div id="react-content">
      </div>
      <script src="${name}.js"></script>
      <script src="${name}.js.map"></script>
      <script type="text/javascript">
        var wpApiSettings = { root: "http://localhost:8080/wp-json/", nonce: "84edf97e92" };
      </script>
    </body>
  </html>
  `;
}

router.get('/test', (ctx) => {
  ctx.body = 'test';
});

router.get('/wp-json/wp/v2/posts', (ctx) => {
  ctx.body = posts25;
});

router.post('/wp-json/wp/v2/posts/:id', (ctx) => {
  ctx.body = editSuccessful;
});

router.delete('/wp-json/wp/v2/posts/:id', (ctx) => {
  ctx.body = editSuccessful;
});

router.get('/', (ctx) => {
  ctx.body = generateHTML('ProPhoto');
});

// apply middleware
app.use(router.routes());
app.use(KoaStatic('build'));

app.listen(port);
console.log(`Now listening on port ${port}`);
